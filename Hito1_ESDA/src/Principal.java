import java.io.*;
import java.util.EmptyStackException;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;
import java.lang.NullPointerException;

public class Principal {
	public static void main (String[]args) throws FileNotFoundException, NullPointerException, EmptyStackException{
		File archivo;
		Queue <Contenedor> contenedor=leer_contenedores(archivo = new File("contenedores.txt"));
		String[] estaciones=leer_estaciones(archivo = new File("estaciones.txt"));
		ZonaColocacion colocacion=new ZonaColocacion();
		colocacion.etiquetar_contenedores(contenedor, estaciones);
		Stack pila_final=colocacion.ordenar(contenedor, estaciones);
		imprimir(pila_final);
		
	}//fin main
	
	private static void imprimir(Stack <Contenedor> pila_final)throws EmptyStackException{
		Contenedor imprimir;
		int cont=0;
		String est=null;
		while(!pila_final.isEmpty()){
			try{
				est=pila_final.peek().get_estacion();
				System.out.println("Estacion: "+est);
				while(est.equalsIgnoreCase(pila_final.peek().get_estacion())){
					imprimir=pila_final.pop();
					System.out.println("Contenedor de la empresa "+imprimir.get_empresa()+" con "+imprimir.get_cantidad()+" unidades de "+imprimir.get_producto()+".");
					cont++;
				}//fin while
			}//fin try
			catch(Exception EmptyStackException){
			}//fin catch
			System.out.println("Numero de contenedores dejados: "+cont+"."+"\n");
			cont=0;
		}//fin while
	}//fin metodo imprimir
	
	private static Queue <Contenedor> leer_contenedores(File archivo)throws FileNotFoundException{
		String estacion, empresa, producto;
		int unidades, pos=0,i=0;
		Scanner datos= new Scanner(archivo);
		while(datos.hasNext()){
			estacion=datos.next();
			empresa=datos.next();
			producto=datos.next();
			unidades=datos.nextInt();
			pos=pos+1;
		}//fin while
		Queue <Contenedor> contenedores=new LinkedBlockingQueue <Contenedor> ();
		datos= new Scanner(archivo);
			while(datos.hasNext()){
				estacion=datos.next();
				empresa=datos.next();
				producto=datos.next();
				unidades=datos.nextInt();
				contenedores.add(new Contenedor(estacion,empresa,producto,unidades));
				i++;
			}//fin while
		datos.close();
		return contenedores;
	}//fin metodo leer_contenedores	
	
	private static String[] leer_estaciones(File archivo)throws FileNotFoundException{
		String estacion;
		int cont=0,pos=0;
		Scanner datos= new Scanner(archivo);
		while(datos.hasNext()){
			estacion=datos.next();
			cont=cont+1;
		}//fin while
		datos= new Scanner(archivo);
		String [] estaciones=new String[cont];
		while(datos.hasNext()){
			estacion=datos.next();
			estaciones[pos]=estacion;
			pos=pos+1;
		}//fin while
		datos.close();
		return estaciones;
	}//fin metodo estaciones

}//fin clase