import java.io.*;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
 public class ZonaColocacion {	
	protected ZonaColocacion(){
		
	}
	
	protected static Stack <Contenedor> ordenar(Queue <Contenedor> contenedores, String []estacion){
		Stack <Contenedor> pila_f=new Stack<Contenedor>();
		etiquetar_contenedores(contenedores, estacion);
		int t_pila=contenedores.size();
		int t_cola=contenedores.size();
		Stack <Contenedor> [] pilas_aux=apilar(contenedores,t_pila,t_cola);
		pila_f=apilar_final(pilas_aux,t_pila,t_cola);
		return pila_f;
	}
	
	protected static Queue <Contenedor> etiquetar_contenedores(Queue <Contenedor> contenedores, String []estacion){
		int r;
		Contenedor elemento;
		for(int i=0;i<contenedores.size();i++){
			r=estacion.length;
			for(int j=0;j<r;j++){
				if(contenedores.peek().get_estacion().equalsIgnoreCase(estacion[j])){
					elemento=contenedores.remove();
					elemento.set_etiquetar(r-1-j);
					contenedores.add(elemento);
				}
			}
		}
		return contenedores;
	}//fin metodo etiquetar_contenedores
	
	protected static Stack <Contenedor> [] apilar(Queue <Contenedor> cola,int t_pila, int t_cola)throws NullPointerException, EmptyStackException{
		Stack <Contenedor> [] pila_parc=new Stack[t_pila];
		for(int i=0;i<t_pila;i++){
			pila_parc[i]=new Stack();
		}
		Contenedor comparar;
		try{
			while(!cola.isEmpty()){
				comparar=cola.remove();
				boolean comprobar=false;
				while(comprobar==false){
					for(int j=0;j<t_pila;j++){
						if(pila_parc[j].isEmpty()){
							pila_parc[j].push(comparar);
							comprobar=true;
							j=t_pila;
						}//fin if
						else{
							if(pila_parc[j].peek().get_etiquetar()<=comparar.get_etiquetar()){
								pila_parc[j].push(comparar);
								comprobar=true;
								j=t_pila;
							}//fin if
						}//fin else
					}//fin for				
				}//fin while
			}//fin-while
			
		}//fin try	
		catch(Exception NullPointerException){
		}
		return pila_parc;
	}//fin metodo vector pila
	
	protected static Stack <Contenedor> apilar_final (Stack <Contenedor> [] pila_parc, int t_pila, int t_cola)throws NullPointerException, EmptyStackException{
		Stack <Contenedor> pila_final=new Stack();
		try{
			int t_pila_etiquetar=t_pila;
			while(pila_final.size()!=t_cola){
				for(int k=0;k<t_pila;k++){
					if(!pila_parc[k].isEmpty()){
						while(pila_parc[k].peek().get_etiquetar()==t_pila_etiquetar){
							pila_final.push(pila_parc[k].pop());
							if(pila_parc[k].isEmpty()){
								k=0;
								while(pila_parc[k].isEmpty()){
									k++;
								}//fin-while
							}//fin if
						}//fin while
					}//fin if
				}//fin for
				t_pila_etiquetar--;
			}//fin while
		}//fin try
		catch(Exception EmptyStackException){
		}//fin catch
		return pila_final;
	}
	
}//fin clase zona colocacion