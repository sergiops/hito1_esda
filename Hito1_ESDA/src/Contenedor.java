
public class Contenedor{
	private String ContEstacion, ContEmpresa, ContProducto;
	private int ContCantidad, ContEtiquetar;	
	protected Contenedor(String estacion, String empresa, String producto, int cantidad){
		ContEstacion=estacion;
		ContEmpresa=empresa;
		ContProducto=producto;
		ContCantidad=cantidad;
	}
	protected String get_estacion(){
		return ContEstacion;
	}
	protected String get_empresa(){
		return ContEmpresa;
	}
	protected String get_producto(){
		return ContProducto;
	}
	protected int get_cantidad(){
		return ContCantidad;
	}
	protected int get_etiquetar(){
		return ContEtiquetar;
	}
	protected void set_estacion(String estacion){
		ContEstacion=estacion;
	}
	protected void get_empresa(String empresa){
		ContEmpresa=empresa;
	}
	protected void set_producto(String producto){
		ContProducto=producto;
	}
	protected void set_cantidad(int cantidad){
		ContCantidad=cantidad;
	}
	protected void set_etiquetar(int id){
		ContEtiquetar=id;
	}//fin etiquetar
	
}
